import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

const Footer = ({subTitle, year}) => {
    const users = [
        { id:1 , name: 'Sam'},
        { id:2 , name: 'Jam'},
        { id:3 , name: 'Yo'},
        { id:4 , name: 'Mam'},
        { id:5 , name: 'Mui'}
    ]
    const [count,setCount] = useState(0);
    const [title,setTitle] = useState('Hello');

    useEffect(()=>{
        console.log('useEffect')
    })

    useEffect(()=>{
        console.log('useEffect 2')
    },[]) //ทำครั้งเดียว เช่นดึงฐานข้อมูลมาครั้งเดียว

    useEffect(()=>{
        console.log('useEffect 3')
    },[title])// ทำก็ต่อเมื่อ title ถูกอัพเดท

    return (
        <View>
            <Text>{subTitle}</Text>
            <Text>พ.ศ. {year+543}</Text>
            <Button 
            title='Click Me'
            onPress={
                () => {
                    setCount(count+1)
                }
            }></Button>
            <Text style={styles.title}>กดกี่ทีดี {count}</Text>
            { 
                users.map( (user,index) => {
                    return <Text key={user.id}>{index+1} {user.name}</Text>
                })
            }
        </View>
    )
}

export default Footer

const styles = StyleSheet.create({
    title: {
        fontSize : 40
    }
})
