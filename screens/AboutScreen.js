import React from 'react'
import { StyleSheet, Text, View , Button} from 'react-native'

const AboutScreen = ({navigation, route}) => {

    const {email} = route.params;
    return (
        <View style={styles.container}>
            <Text>Detail</Text>
            <Text>Email: {email}</Text>
            <Button 
            title="Go Back"
            onPress={()=>{
                navigation.goBack()
            }}
            ></Button>
        </View>
    )
}

export default AboutScreen
//Referencing style
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
