import React from 'react'
import { StyleSheet, View, ScrollView, ImageBackground } from 'react-native'
import { Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';

const MenuScreen = ({navigation}) => {
    return (
        <ScrollView>
            <View>
                <ImageBackground
                style={{
                    flex:1,
                    justifyContent:'center',
                    alignItems:'center',
                    height: 150,
                    width: undefined,
                    
                }}
                source={{
                    uri:'https://image.freepik.com/free-vector/abstract-blue-geometric-shapes-background_1035-17545.jpg'
                }}
                >
                <Text
                style={{
                    color:'blue',
                    fontSize: 20,
                    fontWeight:'bold'
                }}
                >เมนูหลัก</Text>
                </ImageBackground>
            </View>

            <Content>
          <ListItem
          icon={true}
          style={{
            marginBottom:10,
            marginTop:10
            }}
          onPress={
            () => {
              navigation.navigate('HomeStack')
            }
          }>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="home" />
              </Button>
            </Left>
            <Body>
              <Text>หน้าหลัก</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon={true} style={{ marginBottom:10}} onPress={
            () => {
              navigation.navigate('ProductStack')
            }
          }>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="cart-outline" />
              </Button>
            </Left>
            <Body>
              <Text>สินค้า</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
        </ScrollView>
    )
}

export default MenuScreen

const styles = StyleSheet.create({})
